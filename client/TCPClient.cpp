#include "TCPClient.h"
#include <iostream>

void TCPClient::connectServer(int port){
    connection = socket(AF_INET,SOCK_STREAM, 0);
    struct sockaddr_in client_sockaddr;
    memset(&client_sockaddr, 0, sizeof(client_sockaddr));
    client_sockaddr.sin_family = AF_INET;
    client_sockaddr.sin_port = htons(port);
    client_sockaddr.sin_addr.s_addr = htonl(INADDR_ANY);

    int ret = connect(connection, (struct sockaddr*)&client_sockaddr, sizeof(sockaddr_in));
    if(ret < 0){
        perror("error connect to server");
        exit(1);
    }
    printf("connecting to quickstep succeed\n");
}

void TCPClient::disconnectServer(){
    close(connection);
    cout<<"disconnect to quickstep"<<endl;
}

int TCPClient::waitMessageFromServer(){
    char buffer[bufferSize];
    memset(buffer, 0, sizeof(buffer));
    int len = recv(connection, buffer, sizeof(buffer),0);
    if(len <= 0){
        perror("server error");
        exit(1);
    }
    string content = string(buffer);
    cout<<"quickstep send: "<<content<<endl;
    if(content == "RESULT:0")   return 0;
    else    return 1;
}

void TCPClient::sendMessageToServer(const string& message){
    char buffer[bufferSize];
    int length;
    int iter = 0;
    while(iter < message.size()){
        length = (message.size() - iter >= bufferSize) ? bufferSize : message.size() - iter;
        memset(buffer, 0, bufferSize);
        memcpy(buffer, &message[iter], length);
        int len = send(connection, buffer, sizeof(buffer),0);
        if(len == -1){
            perror("error sending message");
            exit(1);
        }
        iter += length;
    }
}

