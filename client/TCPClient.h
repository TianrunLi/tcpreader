#ifndef __TCPCLIENT_H__
#define __TCPCLIENT_H__

// include files for network
#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/shm.h>

#include <string>

using namespace std;

class TCPClient{
private:
    int connection;

    static constexpr int bufferSize = 4096;

public:
    TCPClient(){    connection = -1;    }
    ~TCPClient(){}

    // connect to server
    void connectServer(int port);
    void disconnectServer();

    // send commands to quickstep
    void sendMessageToServer(const string& message);

    // receive message from quickstep, decide if we should quit process
    int waitMessageFromServer();
};

#endif
