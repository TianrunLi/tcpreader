/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 **/

#ifndef QUICKSTEP_CLI_LINE_READER_TCP_SERVER_HPP_
#define QUICKSTEP_CLI_LINE_READER_TCP_SERVER_HPP_

#include "cli/LineReader.hpp"
#include "utility/Macros.hpp"

#include <queue>
#include <vector>

#include <pthread.h>
#include <semaphore.h>

namespace quickstep{

/** \addtogroup CLI
 *  @{
 */

/**
 * @brief An implementation of LineReader that uses TCP to receive
 *        commands from another client
 **/

class LineReaderTCPServer : public LineReader{
public:
    LineReaderTCPServer(const std::string& default_prompt,
                        const std::string& continue_prompt);
    ~LineReaderTCPServer() override;

    // initialize and finalize the command receiver
    void initialize();
    void finalize();

    // send message back to the outer control program
    void sendMessage(const std::string& message);

protected:
    // override this function to build a new reader
    std::string getLineInternal(const bool continuing) override;

private:
    DISALLOW_COPY_AND_ASSIGN(LineReaderTCPServer);

    // string manipulations
    void split(const std::string& str, std::vector<std::string>& ret, char delim);
    int startsWith(const std::string& longStr, const std::string& shortStr);
    std::string& trim_inplace(std::string& s, const std::string& delimiters = " \n\r\t" ){
        s.erase( s.find_last_not_of( delimiters ) + 1 );
        return s.erase( 0, s.find_first_not_of( delimiters ) );
    }

    // functions about initialization
    void createServer();
    void createConnection();
    pthread_t createRecvThread();

    // functions receive and send message
    static void* receiveMessage_helper(void* context);
    void* receiveMessage();

    // add command to queue
    void addToCommandQueue(std::vector<std::string>& lines);

    // command buffer
    std::queue<std::string> commandQueue;

    // semaphore and mutex
    sem_t empty;                // show if a command queue is empty
    pthread_mutex_t queue_mutex;      // access control to commandQueue

    // pthread handle for receive thread
    pthread_t handle;

    // TCP server related things
    int server_sockfd;
    int connection;

    // buffer size
    static constexpr int bufferSize = 4096;

};

/** @} */

}   // namespace quickstep

#endif  // QUICKSTEP_CLI_LINE_READER_TCP_SERVER_HPP_

