/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 **/

#include "cli/LineReaderTCPServer.hpp"

#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/shm.h>

#include <cstdio>
#include <sstream>
#include <vector>

namespace quickstep{

LineReaderTCPServer::LineReaderTCPServer(const std::string& default_prompt,
                                         const std::string& continue_prompt)
    : LineReader(default_prompt, continue_prompt){
        server_sockfd = -1;
        int res = pthread_mutex_init(&queue_mutex, NULL);
        if(res != 0){
            perror("create mutex failed");
            exit(-1);
        }
        res = sem_init(&empty, 0, 0);
        if(res != 0){
            perror("create semaphore failed");
            exit(1);
        }
    }

LineReaderTCPServer::~LineReaderTCPServer(){
    int res = sem_destroy(&empty);
    if(res != 0){
        perror("destroy semaphore failed");
        exit(1);
    }
    res = pthread_mutex_destroy(&queue_mutex);
    if(res != 0){
        perror("destroy mutex failed");
        exit(1);
    }
    close(connection);
    close(server_sockfd);
}

void LineReaderTCPServer::split(const std::string& str, std::vector<std::string>& ret, char delim){
    std::string::size_type pos1 = str.find_first_not_of(delim);
    if(pos1 == std::string::npos)
        return;
    std::string::size_type pos2 = str.find_first_of(delim, pos1);
    while(pos2 != std::string::npos){
        ret.push_back(str.substr(pos1, pos2 - pos1));
        pos1 = str.find_first_not_of(delim, pos2);
        pos2 = str.find_first_of(delim, pos1);
    }
    if(pos1 != std::string::npos)
        ret.push_back(str.substr(pos1));
}

int LineReaderTCPServer::startsWith(const std::string& longStr, const std::string& shortStr){
    return longStr.compare(0, shortStr.size(), shortStr);
}

void* LineReaderTCPServer::receiveMessage_helper(void* context){
    return ((LineReaderTCPServer*)context)->receiveMessage();
}

void* LineReaderTCPServer::receiveMessage(){
    char buffer[bufferSize];
    while(true){
        memset(buffer, 0, sizeof(buffer));
        int len = recv(connection, buffer, sizeof(buffer),0);
        if(len == -1){
            perror("error receiving message");
            exit(1);
        }
        // receive commands, split by \n, add them to command queue
        std::string content = std::string(buffer);
        std::vector<std::string> lines;
        split(content, lines, '\n');
        addToCommandQueue(lines);
        if(content == "quit;"){
            printf("receive message thread quit\n");
            break;
        }
    }
    return NULL;
}

void LineReaderTCPServer::sendMessage(const std::string& message){
    char buffer[bufferSize];
    int length;
    int iter = 0;
    while(iter < int(message.size())) {
        length = message.size() - iter >= bufferSize ? bufferSize : message.size() - iter;
        memset(buffer, 0, bufferSize);
        memcpy(buffer, &message[iter], length);
        int len = send(connection, buffer, sizeof(buffer),0);
        if(len == -1){
            perror("error sending message");
            exit(1);
        }
        iter += length;
    }
}

void LineReaderTCPServer::addToCommandQueue(std::vector<std::string>& lines){
    pthread_mutex_lock(&queue_mutex);

    int counter = 0;
    for(size_t i=0; i<lines.size(); i++){
        trim_inplace(lines[i]);
        // empty line
        if(lines[i].size() == 0)    continue;
        // comment
        if(startsWith(lines[i], "--") == 0) continue;

        // queries are seperated by ';', but commands are seperated by '\n', like \analyze
        // so, to support commands like \analyze, add a '\n' at the end of each line
        lines[i].append("\n");
        commandQueue.push(lines[i]);
        counter++;
    }
    for(int i=0; i<counter; i++)
        sem_post(&empty);

    pthread_mutex_unlock(&queue_mutex);
}

void LineReaderTCPServer::createServer(){
    server_sockfd = socket(AF_INET,SOCK_STREAM, 0);
    struct sockaddr_in server_sockaddr;
    server_sockaddr.sin_family = AF_INET;
    server_sockaddr.sin_port = 0;       // select an available port
    server_sockaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    if(bind(server_sockfd,(struct sockaddr *)&server_sockaddr,sizeof(server_sockaddr))==-1){
        perror("error during bind");
        exit(1);
    }
    if(listen(server_sockfd,1) == -1){
        perror("error during listen");
        exit(1);
    }
    // print the port for this server
    socklen_t sin_size = sizeof(server_sockaddr);
    getsockname(server_sockfd, (struct sockaddr*)&server_sockaddr, &sin_size);
    int port=ntohs(server_sockaddr.sin_port);
    printf("server running on port: %d\n", port);
}

void LineReaderTCPServer::createConnection(){
    struct sockaddr_in client_addr_recv;
    socklen_t length = sizeof(sockaddr_in);
    connection = accept(server_sockfd, (struct sockaddr*)&client_addr_recv, &length);
    if(connection<0){
        perror("error during create connection");
        exit(1);
    }
    printf("create connection\n");
}

pthread_t LineReaderTCPServer::createRecvThread(){
    pthread_t handle;
    int ret = pthread_create(&handle, NULL, &LineReaderTCPServer::receiveMessage_helper, this);
    if(ret != 0){
        perror("error during create recv thread");
        exit(1);
    }
    printf("create the recv thread\n");
    return handle;
}

void LineReaderTCPServer::initialize(){
    createServer();
    createConnection();
    handle = createRecvThread();
}

void LineReaderTCPServer::finalize(){
    pthread_join(handle, NULL);
}

std::string LineReaderTCPServer::getLineInternal(const bool continuing){
    sem_wait(&empty);
    pthread_mutex_lock(&queue_mutex);

    std::string ret = commandQueue.front();
    commandQueue.pop();

    pthread_mutex_unlock(&queue_mutex);
    return ret;
}


}   // namespace quickstep

