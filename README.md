# TCP Reader for Quickstep #

This is a simple command reader of Quickstep using TCP.  
It can support for loop and if condition in Quickstep.  

### How to use this code ###

For the command reader in Quickstep:  

1. copy all the files in quickstep\_cli directory to quickstep/cli, this will overwrite CMakeList.txt and QuickstepCli.cpp  
2. re-make the project  
3. start quickstep as usual, now it will listen to commands on a port  

For the control program outside Quickstep:   

1. include TCPClient.h in your own code to send/receive message from Quickstep  
2. Initialize a TCPClient in your code, with the corresponding port of Quickstep  
3. Write your own for loop and if confidion, and use sendMessageToServer to send command to Quickstep  
4. If a query is a SELECT query, quickstep will send back the number of rows of the query result, use waitMessageFromServer to receive them  
5. Send a 'quit;' command to Quickstep at the end of your program, to make sure that quickstep exits normally  

### How it works ###

1. The TCPLineReader in Quickstep creates a new thread to receive message from outer program, so it can receive commands anytime during the execution  
2. The TCPClient is currently single thread. So, if it is a SELECT query and quickstep sends back the number of rows of query result, TCPClient need to wait for the result  

### Updates in QuickstepCli.cpp ###

To use this TCP Reader, I made several updates in QuickstepCli.cpp  
All the updates are marked by '// code updated by Tianrun'  
You can check them by this mark  
